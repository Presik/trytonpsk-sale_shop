# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import shop
from . import sale
from . import user
from . import configuration
from . import stock
from . import invoice
from . import price_list
from . import product
from . import source
from . import statement


def register():
    Pool.register(
        configuration.Configuration,
        shop.SaleShop,
        shop.SaleShopResUser,
        shop.SaleShopEmployee,
        user.User,
        sale.Sale,
        statement.Journal,
        statement.StatementLine,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        sale.SaleBySupplierStart,
        sale.SaleLine,
        sale.SaleShopDetailedStart,
        sale.SalePaymentForm,
        invoice.Invoice,
        invoice.InvoicesStart,
        price_list.PriceListLine,
        product.UpdatePriceProductStart,
        product.Template,
        source.SaleSource,
        price_list.PriceListBySupplierStart,
        sale.SaleBySellerStart,
        sale.SaleMonthByShopStart,
        sale.MultiplePaymentSaleStart,
        sale.SelectMultiplePaymentSale,
        sale.SelectSalesAsk,
        # goal.SaleIndicator,
        # goal.SaleGoalLine,
        # goal.Context,
        module='sale_shop', type_='model')
    Pool.register(
        sale.PrintSaleBySupplier,
        sale.SaleBySeller,
        sale.SaleShopDetailed,
        invoice.Invoices,
        product.UpdatePriceProduct,
        price_list.PriceListBySupplier,
        sale.SaleMonthByShop,
        sale.WizardSalePayment,
        sale.MultiplePaymentSale,
        # sale.SelectLines,
        module='sale_shop', type_='wizard')
    Pool.register(
        sale.SaleBySupplier,
        sale.SaleBySellerReport,
        sale.SaleShopDetailedReport,
        invoice.InvoicesReport,
        # price_list.ProductPriceList,
        price_list.PriceListBySupplierReport,
        sale.SaleMonthByShopReport,
        module='sale_shop', type_='report')
