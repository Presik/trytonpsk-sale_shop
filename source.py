# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction

STATES = {
        'readonly': (Eval('state') != 'draft')
    }

SOURCES = [
    ('', ''),
    ('mercadolibre', 'Mercado Libre'),
    ('rappi', 'Rappi'),
    ('melhous', 'Melhous'),
    ('ifood', 'IFood'),
    ('shopify', 'Shopify'),
    ('socialnetwork', 'Social Network'),
    ('direct', 'Direct'),
    ('webpage', 'Web Page'),
    ('internal', 'Internal'),
    ('platform', 'Platform'),
    ('telephone', 'Telephone'),
]

# Migration form web channel
# INSERT INTO sale_source (id, name, "type", active, company) SELECT id, name, channel_name, true, 1 FROM sale_web_channel;


class SaleSource(Workflow, ModelSQL, ModelView):
    'Sale Source'
    __name__ = 'sale.source'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
    type = fields.Selection(SOURCES, 'Type', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    party = fields.Many2One('party.party', 'Party')
    payment_term = fields.Many2One('account.invoice.payment_term',
       'Payment Term')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_active():
        return True
