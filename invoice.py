# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    shop = fields.Many2One('sale.shop', 'Shop', required=False, domain=[
            ('id', 'in', Eval('context', {}).get('shops', [])),
            ], depends=['number'], states={'invisible': Eval('type') != 'out'})


class InvoicesStart(metaclass=PoolMeta):
    __name__ = 'invoice_report.print_invoices.start'
    shop = fields.Many2One('sale.shop', 'Shop', required=False)


class Invoices(metaclass=PoolMeta):
    __name__ = 'invoice_report.print_invoices'

    def do_print_(self, action):
        action, data = super(Invoices, self).do_print_(action)
        if self.start.shop:
            data['shop'] = self.start.shop.id
            data['shop_name'] = self.start.shop.name
        return action, data


class InvoicesReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.invoices_report'

    @classmethod
    def get_domain_invoices(cls, data):
        dom_invoices = super(InvoicesReport, cls).get_domain_invoices(data)
        if data.get('shop'):
            dom_invoices.append(('shop', '=', data['shop']))
        return dom_invoices
