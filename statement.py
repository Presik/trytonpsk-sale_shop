# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.i18n import gettext
from trytond.exceptions import UserError

_ZERO = Decimal("0.0")

PAYMENT_CODES = [
    ('', ''),
    ('1', 'Instrumento no definido'),
    ('10', 'Efectivo'),
    ('44', 'Nota Cambiaria'),
    ('20', 'Cheque'),
    ('48', 'Tarjeta Crédito'),
    ('49', 'Tarjeta Débito'),
    ('42', 'Consignación bancaria'),
    ('47', 'Transferencia Débito Bancaria'),
    ('45', 'Transferencia Crédito Bancaria'),
]


class Journal(metaclass=PoolMeta):
    __name__ = 'account.statement.journal'
    payment_means_code = fields.Selection(
        PAYMENT_CODES, 'Payment Means Code', required=True)
    kind = fields.Selection([
            ('cash', 'Cash'),
            ('electronic', 'Electronic'),
            ('transfer', 'Transfer'),
            ('payment', 'Payment'),
            ('other', 'Other'),
        ], 'Kind')
    default_start_balance = fields.Numeric('Default Start Balance by Device')
    require_voucher = fields.Boolean('Require Voucher')
    require_party = fields.Boolean('Require Party')
    party = fields.Many2One('party.party', 'Party',
        states={'invisible': ~Eval('require_party')})

    @staticmethod
    def default_kind():
        return 'cash'

    @staticmethod
    def default_require_party():
        return False


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    sale = fields.Many2One('sale.sale', 'Sale', ondelete='RESTRICT')
    voucher = fields.Char('Voucher Number')

    def get_move_line2(self, values):
        'Return counterpart Move Line for the amount'
        move_id = values['move_id']
        journal_account = values.get('journal_account', False)
        account = values.get('account')
        amount = values.get('amount')
        credit = _ZERO
        debit = _ZERO
        if journal_account:
            if amount >= 0:
                debit = amount
            else:
                credit = abs(amount)
        else:
            if amount >= 0:
                credit = amount
            else:
                debit = abs(amount)

        if not account:
            raise UserError(
                gettext('sale_pos.msg_account_statement_journal',
                s=self.journal.rec_name))
        res = {
            'description': self.description,
            'debit': debit,
            'credit': credit,
            'account': account.id,
            'party': self.party.id if account.party_required else None,
            'move': move_id,
        }
        return res

    def _get_move2(self):
        # Return Move for the grouping key
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Period = pool.get('account.period')

        company_id = self.statement.company.id
        period_id = Period.find(company_id, date=self.date)
        _move = {
            'period': period_id,
            'journal': self.statement.journal.journal.id,
            'date': self.date,
            'origin': str(self.statement),
            'company': company_id,
            'state': 'draft',
        }

        move, = Move.create([_move])

        move_line1 = self.get_move_line2({
            'move_id': move.id,
            'account': self.account,
            'amount': self.amount
        })
        extra_amount = 0
        if hasattr(self, 'extra_amount') and self.extra_amount:
            extra_amount = self.extra_amount

        move_line2 = self.get_move_line2({
            'account': self.statement.journal.account,
            'move_id': move.id,
            'amount': self.amount + extra_amount,
            'journal_account': True,
        })
        if self.statement.journal.require_party and self.statement.journal.party:
            party = self.statement.journal.party.id
            move_line2['party'] = party
        elif self.statement.journal.journal.type == 'commission':
            party = self.sale.agent.party.id
            move_line2['party'] = party
        to_create = [move_line1, move_line2]

        journals = self.statement.sale_device.journals if hasattr(self.statement, 'sale_device') else []
        journals = [j for j in journals if j.kind == 'cash']

        # Just add extra amount to move line when payment is different to cash
        if journals:
            journal = journals[0]
            if hasattr(self, 'extra_amount') and self.extra_amount and self.extra_amount > 0 and \
                    self.statement.journal.id != journal.id:
                    move_line3 = self.get_move_line2({
                        'account': journal.account,
                        'move_id': move.id,
                        'amount': self.extra_amount
                    })
                    to_create.append(move_line3)
        MoveLine.create(to_create)
        return move

    @classmethod
    def post_move(cls, lines):
        super(StatementLine, cls).post_move(lines)
        for s in lines:
            if s.invoice and s.move and s.invoice.state != 'paid':
                invoice = s.invoice
                move = s.move
                payment_lines = [
                    l for l in move.lines if l.account == invoice.account and l.party == invoice.party
                ]
                invoice.add_payment_lines({invoice: payment_lines})

    def create_move(self):
        # Overwrite sale_pos > create_move
        move = self._get_move2()
        self.write([self], {'move': move.id})
