# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import itertools
from trytond.model import fields, ModelView
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import PoolMeta, Pool
from trytond.modules.product import price_digits
from trytond.transaction import Transaction
from psycopg2.extras import DictCursor
from decimal import Decimal


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    def __init__(self, arg):
        super(Product, self).__init__()
        self.arg = arg  # ??????


class UpdatePriceProductStart(ModelView):
    'Update Price Product Start'
    __name__ = 'product.update_product.start'
    product = fields.Many2Many('product.template', None, None, 'Products')
    sale_price = fields.Numeric('Sale Price', digits=price_digits)
    sale_price_w_tax = fields.Numeric('Sale Price With Tax',
        digits=price_digits)

    @fields.depends('sale_price', 'sale_price_w_tax')
    def on_change_sale_price(self, name=None):
        self.sale_price_w_tax = None

    @fields.depends('sale_price', 'sale_price_w_tax')
    def on_change_sale_price_w_tax(self, name=None):
        self.sale_price = None


class UpdatePriceProduct(Wizard):
    'Update Price Product'
    __name__ = 'product.update_product'
    start = StateView(
        'product.update_product.start',
        'sale_shop.update_product_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Update', 'update', 'tryton-ok', default=True),
        ])
    update = StateTransition()

    def transition_update(self):
        pool = Pool()
        ProductTemplate = pool.get('product.template')
        sale_price_w_tax = self.start.sale_price_w_tax
        sale_price = self.start.sale_price
        if sale_price or sale_price_w_tax:
            for product in self.start.product:
                if sale_price_w_tax:
                    list_price = product.compute_reverse_list_price(sale_price_w_tax)
                    ProductTemplate.write([product], {
                        'sale_price_w_tax': sale_price_w_tax,
                        'list_price': list_price
                        })
                if sale_price:
                    list_price = product.compute_list_price_w_tax(sale_price)
                    ProductTemplate.write([product], {
                        'sale_price_w_tax': list_price,
                        'list_price': sale_price
                        })
        return 'end'


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    sale_price_w_tax = fields.Numeric('Sale Price With Tax', digits=(16, 2),
        depends=['list_price', 'account_category'])
    profit = fields.Function(fields.Float('Profit',
        digits=(16, 2)), 'get_profit')
    list_prices_line = fields.Function(fields.Many2Many(
        'product.price_list.line', None, None, 'List Price'),
        'get_list_prices'
    )

    @fields.depends('list_price', 'sale_price_w_tax', 'account_category')
    def on_change_account_category(self, name=None):
        if self.sale_price_w_tax:
            res = self.compute_reverse_list_price(self.sale_price_w_tax)
            if res:
                self.list_price = res
            else:
                self.list_price = self.sale_price_w_tax
        else:
            self.sale_price_w_tax = self.list_price

    # @fields.depends('list_price', 'account_category', 'sale_price_w_tax')
    # def on_change_with_list_price(self, name=None):
    #     res = self.
    #     if self.sale_price_w_tax:
    #         # self.on_change_account_category()
    #         res = self.compute_reverse_list_price(self.sale_price_w_tax)
    #         if not res and res != 0:
    #             res = self.sale_price_w_tax
    #     print('on_change_with_list_price')
    #     return res

    @fields.depends('list_price', 'account_category', 'sale_price_w_tax')
    def on_change_list_price(self, name=None):
        if self.list_price:
            self.sale_price_w_tax = self.compute_list_price_w_tax(self.list_price)

    @fields.depends('list_price', 'account_category', 'sale_price_w_tax')
    def on_change_sale_price_w_tax(self, name=None):
        self.on_change_account_category()

    def compute_list_price_w_tax(self, list_price):
        Tax = Pool().get('account.tax')
        res = list_price
        if self.customer_taxes_used and list_price:
            tax_list = Tax.compute(self.customer_taxes_used,
                list_price or Decimal('0.0'), 1)
            res = sum([t['amount'] for t in tax_list], Decimal('0.0'))
            res = res + list_price
        res = Decimal(res).quantize(
            Decimal(1) / 10 ** self.__class__.sale_price_w_tax.digits[1])
        return res

    def compute_reverse_list_price(self, price_w_tax):
        Tax = Pool().get('account.tax')
        if hasattr(self, 'customer_taxes_used'):
            res = Tax.reverse_compute(price_w_tax, self.customer_taxes_used)
            res = res.quantize(
                Decimal(1) / 10 ** self.__class__.list_price.digits[1])
            return res

    def get_profit(self, name=None):
        if self.cost_price and self.list_price and self.cost_price != 0:
            res = float(1-(self.cost_price/self.list_price)) * 100  # villa
            return round(res, 2)

    def get_list_prices(self, name=None):
        PriceList = Pool().get('product.price_list.line')
        price_list_line = PriceList.__table__()
        dict_cursor = Transaction().connection.cursor(cursor_factory=DictCursor)
        if self.products:
            query = price_list_line.select(price_list_line.id,
                where=(price_list_line.product.in_([p.id for p in self.products]))
                )
            dict_cursor.execute(*query)
            result = dict_cursor.fetchall()
            return list(itertools.chain(*result))
