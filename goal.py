# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta

# class SaleGoalLine(metaclass=PoolMeta):
#     "Sale Goal Line"
#     __name__ = 'sale.goal.line'
#
#     @classmethod
#     def __setup__(cls):
#         super(SaleGoalLine, cls).__setup__()
#         cls.kind.selection.extend([('shop', 'Shop')])
#
#
# class SaleIndicator(metaclass=PoolMeta):
#     "Sale Indicator"
#     __name__ = 'sale.indicator'
#     shop = fields.Many2One('sale.shop', 'Shop')


class Context(metaclass=PoolMeta):
    "Goal Reporting Context"
    __name__ = 'goal.reporting.context'

    @classmethod
    def __setup__(cls):
        super(Context, cls).__setup__()
        cls.kind.selection.extend([('shop', 'Shop')])

    # @fields.depends('shop')
    # def on_change_kind(self):
    #     super(SaleIndicator, self)
    #     self.shop = None
    #
    # @fields.depends('kind', 'name', 'shop')
    # def on_change_shop(self):
    #     if self.kind == 'shop' and self.shop:
    #         self.name = self.shop.name
#
#
# class SaleGoalAnnualRankingStart(metaclass=PoolMeta):
#     'Goal Annual Ranking Start'
#     __name__ = 'sale_goal.annual_ranking.start'
#
#     shop = fields.Many2One('sale.shop', 'Shop', states={
#         'required': Eval('indicator') == 'by_category',
#         'invisible': Eval('indicator') != 'by_category'
#     })
#
#     @classmethod
#     def __setup__(cls):
#         super(SaleIndicator, cls).__setup__()
#         cls.indicator.selection.extend([('shop', 'Shop')])
#
#     @staticmethod
#     def default_indicator():
#         return 'shop'
#
#     def do_print_(self, action):
#         action, data = super(SaleGoalAnnualRankingStart, self).do_print_(action)
#         shop_id = None
#         if self.start.shop:
#             shop_id = self.start.shop.id
#
#         data.update({'shop': shop_id})
#         return action, data
